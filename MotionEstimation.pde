import processing.video.*; //<>// //<>// //<>//

// define parameteres
boolean DEBUG = false;
boolean SHOW_MAGNITUDE = false;
boolean SHOW_MAGNITUDE_1pixel = false;
boolean SHOW_VECTOR = true;
boolean SHOW_GRIDLINE = false;
//
boolean IGNORE_BLUE = true; // ignore blue color (possibly to ignore background color)
boolean IGNORE_BLACK = false; // ignore black color (possibly to ignore background color)
//============= USER SETTING =============//
int K = 15; // BlockSize
int R = 5; // searching radius
float DIFFERENT_PERCENTAGE = 0.05; // Control the minimum pecentage of differences (SSD) required to consider as a grid had moved.
int MAGNITUDE_THRESHOLD = 0; // minimum threshold to show the magnitude
boolean UNIFORM_MAGNITUDE = false; //To control whether use a single color to display magnitude (when TRUE), or display different magnitude value (when FALSE)
//
//=========== GLOBAL VARIABLE ===========//
Movie m;
PImage prev;
PImage cur;
int[][][] dispVector;
float[][] dispMagnitude;
float dispMax;

///////////////////////////////////////////

void setup() {
  //=========== SETUP MOVIE FILE ===========//
  
  size(1280, 720);
  m = new Movie(this, sketchPath("star_trails.mov"));
    
  //size(720, 576);
  //m = new Movie(this, sketchPath("monkey.avi"));  

  //size(320, 232);
  //m = new Movie(this, sketchPath("Moving Ball.mp4"));
  
  //size(480, 360);
  //m = new Movie(this, sketchPath("Bear.mp4"));  
  //m.speed(0.4);
  
  m.loop();

  cur = createImage(width, height, RGB);
  prev = createImage(width, height, RGB);
  prev.copy(m, 0, 0, width, height, 0, 0, width, height);
  //size(165 , 100);
  //prev = loadImage(sketchPath("test_c_1.png"));
  //cur = loadImage(sketchPath("test_c_2.png"));
}

void draw() {
 
  if (m.available()){
      m.read();

      // the displacement vector & magnitude array:
      dispVector = new int[width][height][2];
      dispMagnitude = new float[width][height];

      cur.copy(m, 0, 0, width, height, 0, 0, width, height);
      
      //call method to analysis the current and previous frame:
      dispMax = analysisMotion(dispVector, dispMagnitude);
      
      image(prev, 0, 0);
      
      if (SHOW_MAGNITUDE){
          if(!SHOW_MAGNITUDE_1pixel){
            background(0);
          }
          PImage mag = displayMagnitude(prev, dispMagnitude, dispMax);
          image(mag, 0, 0);
      }
      

      

      
      if(SHOW_VECTOR){      
          stroke(255,0,0); // draw red arrow
          displayVector(dispVector);
      }
      
      if(SHOW_GRIDLINE){
          stroke(0); // draw black grid line
          drawGrid();
      }
      
      prev.copy(m, 0, 0, width, height, 0, 0, width, height);
    
      //noLoop();
  }
}

float analysisMotion(int [][][] dispVector, float [][]dispMagnitude) {
  prev.loadPixels();
  cur.loadPixels();
  
  float dispMax = 0; // keeping track the maximum displacement to show the magnitude:

  // For each grid block:
  for (int iy = 0; iy < height / K; ++iy) {
    for (int ix = 0; ix < width / K; ++ix) {
      
      // FIRST get origin location ssd (ie no shift):
      float ori_ssd = 0;
      for (int jx = ix * K; jx < (ix + 1) * K; ++jx) { 
          for (int jy = iy * K; jy < (iy + 1) * K; ++jy) {
              int ori_loc = jy * width + jx;
              ori_ssd += get_ssd_helper(cur.pixels[ori_loc], prev.pixels[ori_loc]);
          }
      }
      ori_ssd = sqrt(ori_ssd);
      
      float minSSD = ori_ssd;
      int deltaX = 0;
      int deltaY = 0;
      
      for (int shiftY = -R; shiftY <= R; ++shiftY) {
        for (int shiftX = -R; shiftX <= R; ++shiftX) {
          //skip zero shift (as it had already been checked)
          if (shiftX == 0 || shiftY == 0){ 
              continue;
          }
          //skip if it is out of bound:
          if(!inRange(ix*K + shiftX, 0, width-1 ) || !inRange((ix+1)*K + shiftX - 1, 0, width-1 ) || 
             !inRange(iy*K + shiftY, 0, height-1) || !inRange((iy+1)*K + shiftY - 1, 0, height-1) ){
              continue;
          }
          
          // check the SSD for all shift configuration
          float ssd = 0;
          int ref_loc, search_loc;
          // loop through all within the grid
          for (int jy = iy * K; jy < (iy + 1) * K; ++jy) {
            for (int jx = ix * K; jx < (ix + 1) * K; ++jx) { 
                  ref_loc = jy  * width + jx;
                  search_loc = (jy + shiftY) * width + (jx + shiftX);
                  ssd += get_ssd_helper(cur.pixels[search_loc], prev.pixels[ref_loc]);
              }
          } //<>//
          ssd = sqrt(ssd);
          // to find minimum ssd
          if (minSSD > ssd) {
            // require a minimal differences of SSD compare to original location
            float diff_percent = DIFFERENT_PERCENTAGE; //requires at least X% differences (different from original)
            if(ori_ssd > 0 && abs(ori_ssd - ssd) >= ori_ssd * diff_percent){
              minSSD = ssd;
              deltaX = shiftX;
              deltaY = shiftY;

            }
          }
        }
      }

      // for the coor with minimum ssd
      dispVector[ix][iy][0] = deltaX; //<>//
      dispVector[ix][iy][1] = -deltaY; // (the coordinate system in maths, y-axis is flipped compare to pixel coordinate. hence need to negate it) 
      dispMagnitude[ix][iy] = sqrt(deltaX * deltaX + deltaY * deltaY);
      if (DEBUG && (deltaX != 0 || deltaY != 0)){
          println("ori: " + ori_ssd);
          println("cur: " + minSSD);
          println("from: " + ix + "," +iy + " to: " + (ix+deltaX) + "," + (iy-deltaY) ); 
          println("accuracy: " + (100-(float)minSSD/ori_ssd*100) + "%");
      }
      if (dispMax < dispMagnitude[ix][iy]) {
        dispMax = dispMagnitude[ix][iy];
      }
    }
  }
  return dispMax;
}

float get_ssd_helper(color c1, color c2){ //helper function to retrieve two colors' r,g,b value, and return the ssd value:
    int r1 = (c1 >> 16) & 0xFF;  // Faster way of getting red(argb)
    int g1 = (c1 >> 8) & 0xFF;   // Faster way of getting green(argb)
    int b1 = (c1 & 0xFF);        // Faster way of getting blue(argb)
    int r2 = (c2 >> 16) & 0xFF;  // Faster way of getting red(argb)
    int g2 = (c2 >> 8) & 0xFF;   // Faster way of getting green(argb)
    int b2 = (c2 & 0xFF);        // Faster way of getting blue(argb)
    int r = r1-r2;
    int g = g1-g2;
    int b = b1-b2;
    return r*r + g*g + b*b;
}

void displayVector(int[][][] dispVector) {
  for (int iy = 0; iy < height / K; ++iy) {
    for (int ix = 0; ix < width / K; ++ix) {
      // get middle of grid:
      int x = ix*K + ceil(K/2); 
      int y = iy*K + ceil(K/2);
      int loc = y * width + x;
      
      if(IGNORE_BLUE && (blue(prev.pixels[loc]) >200)){ // ignore blue
          continue;
      }
      if(IGNORE_BLACK && (blue(prev.pixels[loc]) >200 && red(prev.pixels[loc]) > 200 && green(prev.pixels[loc]) > 200 )){ // ignore black
          continue;
      }
      // for each grid, display the direction with arrow
      float deltaX = dispVector[ix][iy][0];
      float deltaY = dispVector[ix][iy][1];
      if (deltaX == 0 || deltaY == 0){ // only draw when necessary
          continue;
      }
      int len = ceil(sqrt(deltaX * deltaX + deltaY * deltaY));
      drawArrow(x,y,len,ceil(deltaX),ceil(deltaY));
    }
  }
}

void drawArrow(int cx, int cy, int len, int deltaX, int deltaY){
  strokeWeight(2);
  int arrowHeadSize = 2;
  float rad = atan2(-deltaY,deltaX);
  pushMatrix();
  translate(cx, cy);
  //rotate(radians(angle));   <=== need to conver to radians if decessary
  rotate(rad);
  line(0,0,len, 0);
  line(len, 0, len - arrowHeadSize, -arrowHeadSize);
  line(len, 0, len - arrowHeadSize, arrowHeadSize);
  popMatrix();
}

PImage displayMagnitude(PImage img, float[][] dispMagnitude, float dispMax) {
  PImage mag = createImage(width, height, ARGB);
  img.loadPixels();
  for (int iy = 0; iy < height / K; ++iy) {
    for (int ix = 0; ix < width / K; ++ix) {

      // CHECK IF NEED TO IGNORE THIS GRID (for ignoring background)
      if(IGNORE_BLUE || IGNORE_BLACK){
          int x = ix*K + ceil(K/2); 
          int y = iy*K + ceil(K/2);
          int loc = y * width + x;
          if(IGNORE_BLUE && (blue(prev.pixels[loc]) >200)){ // ignore blue
              continue;
          }
          if(IGNORE_BLACK && (blue(prev.pixels[loc]) >200 && red(prev.pixels[loc]) > 200 && green(prev.pixels[loc]) > 200 )){ // ignore black
              continue;
          }
      }
      if(SHOW_MAGNITUDE_1pixel){
        // only show magnitude at the very middle
        int x = ix*K + ceil(K/2); 
        int y = iy*K + ceil(K/2);
        int loc = y * width + x;
        int magnitude = (int)(dispMagnitude[ix][iy] / dispMax * 255);
        if (magnitude >= MAGNITUDE_THRESHOLD){
          mag.pixels[loc] = color(255);
        }
      }else{
        for (int jy = iy * K; jy < (iy + 1) * K && jy < height; ++jy) {
          for (int jx = ix * K; jx < (ix + 1) * K && jx < width; ++jx) {
            int loc = jy * width + jx;
            // get magnitude
            int magnitude = (int)(dispMagnitude[ix][iy] / dispMax * 255);
            if (magnitude >= MAGNITUDE_THRESHOLD){
              if (UNIFORM_MAGNITUDE){
                mag.pixels[loc] = color(255);
              }else{
                mag.pixels[loc] = color(magnitude);
              }
            }
          }
        }
      }
    }
  }
  mag.updatePixels();
  return mag;
}

void drawGrid() {
  strokeWeight(1);
  for (int i = 0; i <= height/K; ++i) {
      line(0, i*K, width/K*K, i*K);
  }
  for (int j = 0; j <= width/K; ++j) {
      line(j*K, 0, j*K, height/K*K);
  }
}

// helper function to check range
boolean inRange(int value, int min, int max) {
   return (value>= min) && (value<= max);
}

// Called every time a new frame is available to read
void movieEvent(Movie m) {
}
